﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    Rigidbody2D rgb;
    Animator anim;
    float max_vel = 5f;
    bool haciaDerecha = true;
    bool saltando = false;
    float GRAVEDAD = 1;

	public float impulso_salto = 10f;
	// Use this for initialization
	void Start () {
        rgb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        float v = Input.GetAxis("Horizontal");
        float h = Input.GetAxis("Vertical");

        if (h>0 && !saltando)
        {
            Jump();            
        }

        Vector2 vel = new Vector2(v*max_vel, rgb.velocity.y);
        rgb.velocity = vel;
        anim.SetFloat("speed", Mathf.Abs(v));

        if (haciaDerecha && v < 0)
        {
            haciaDerecha = false;
            Flip();
        }
        else if (!haciaDerecha && v > 0)
        {
            haciaDerecha = true;
            Flip();
        }

        bool atacar = Input.GetKeyDown(KeyCode.Space);

        if (atacar)
        {
            anim.SetTrigger("attack");
        }

	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Suelo" && saltando)
        {
            saltando = false;
        }
    }

    void Jump()
    {
        saltando = true;
		rgb.AddForce(new Vector2(0, impulso_salto), ForceMode2D.Impulse);
    }

    void Flip()
    {
        var ls = transform.localScale;
        ls.x *= -1;
        transform.localScale = ls;
    }
}
