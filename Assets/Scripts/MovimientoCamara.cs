﻿using UnityEngine;
using System.Collections;

public class MovimientoCamara : MonoBehaviour {

    public GameObject follow_player;
    Camera cam;
    Transform pt;

	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();                
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 player_pos = new Vector3(follow_player.transform.position.x, -3.83f, transform.position.z);        
        transform.position = player_pos;
	}
}
