﻿using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {

    public float vel = -1f;    
    Rigidbody2D rb2d;
    Animator anim;

    // Use this for initialization
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (anim.GetCurrentAnimatorStateInfo(0).IsName("Caminando") && Random.value < 1f/(60f * 3f)) {
            anim.SetTrigger("apuntar");
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Apuntando")) {
            if (Random.value < 1f / 3f) {
                anim.SetTrigger("disparar");
            } else {
                anim.SetTrigger("caminar");
            }

        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Flip();
    }

    void Flip()
    {        
        var ls = transform.localScale;
        ls.x *= -1;
        transform.localScale = ls;
        vel *= -1;
    }

    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(vel, 0);
    }
}
